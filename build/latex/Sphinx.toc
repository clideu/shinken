\select@language {english}
\contentsline {chapter}{\numberline {1}CentOS 7.0}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Installation}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Serveur avec GUI}{1}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Alternative : Serveur sans GUI}{1}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Installation des derni\IeC {\`e}res mise \IeC {\`a} jour}{1}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}CentOS minimal version - Installation d'applications utiles}{1}{subsection.1.1.4}
\contentsline {section}{\numberline {1.2}Configuration}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}D\IeC {\'e}sactiver SELinux}{2}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}CentOS minimal version - Utilisateur shinken}{2}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Installation et configuration du client NTP}{2}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Remplacer firewalld par iptables}{3}{subsection.1.2.4}
\contentsline {section}{\numberline {1.3}D\IeC {\'e}sactiver IPv6 sur toutes les interfaces}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}D\IeC {\'e}finition d'une adresse IP statique}{4}{section.1.4}
\contentsline {chapter}{\numberline {2}Shinken 2.4}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}D\IeC {\'e}finition}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Installation}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Installation des d\IeC {\'e}pendances li\IeC {\'e}s \IeC {\`a} Shinken}{7}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Installation de PIP, puis de Shinken (2.4) via PIP}{7}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Configuration}{8}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Ajouter Shinken au d\IeC {\'e}marrage de CentOS}{8}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}D\IeC {\'e}marrer Shinken}{8}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Liste des commandes disponible avec Shinken}{8}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Initialiser Shinken}{8}{subsection.2.3.4}
\contentsline {section}{\numberline {2.4}Probl\IeC {\`e}me avec Shinken 2.4: Apr\IeC {\`e}s reboot, le service ne d\IeC {\'e}marre pas}{8}{section.2.4}
\contentsline {chapter}{\numberline {3}MongoDB 3.0}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}D\IeC {\'e}finition}{11}{section.3.1}
\contentsline {section}{\numberline {3.2}Installation}{11}{section.3.2}
\contentsline {section}{\numberline {3.3}Configuration}{12}{section.3.3}
\contentsline {chapter}{\numberline {4}Modules compl\IeC {\'e}mentaires Shinken}{13}{chapter.4}
\contentsline {section}{\numberline {4.1}webui}{14}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Description}{14}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Installation}{14}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Configuration}{14}{subsection.4.1.3}
\contentsline {subsubsection}{Ouverture du port du pare-feu}{14}{subsubsection*.3}
\contentsline {subsubsection}{Installation et configuration du module auth-cfg-password}{15}{subsubsection*.4}
\contentsline {subsubsection}{Installation et configuration du module sqlite}{15}{subsubsection*.5}
\contentsline {section}{\numberline {4.2}webui2}{16}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Description}{16}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Installation}{16}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Configuration}{17}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Monitoring-Plugins}{17}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Description}{17}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Installation}{18}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Configuration}{18}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}linux-snmp}{19}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Description}{19}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Installation du module}{19}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Ajout du service au fichier de configuration de l'h\IeC {\^o}te}{19}{subsection.4.4.3}
\contentsline {section}{\numberline {4.5}linux-ssh}{19}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Description}{19}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Installation du module}{20}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Configuration du module}{20}{subsection.4.5.3}
\contentsline {subsubsection}{Ajout du service au fichier de configuration de l'h\IeC {\^o}te}{21}{subsubsection*.6}
\contentsline {chapter}{\numberline {5}Graphite 0.9.10}{23}{chapter.5}
\contentsline {section}{\numberline {5.1}D\IeC {\'e}finition}{23}{section.5.1}
\contentsline {section}{\numberline {5.2}Installation}{24}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Installation des d\IeC {\'e}pendances (via YUM)}{24}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Installation des d\IeC {\'e}pendances (via PIP)}{24}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}T\IeC {\'e}l\IeC {\'e}chargement et installation de mod\_python 3.5.0}{24}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}T\IeC {\'e}l\IeC {\'e}chargement et installation de txAMQP v.0.6.2}{26}{subsection.5.2.4}
\contentsline {subsection}{\numberline {5.2.5}T\IeC {\'e}l\IeC {\'e}chargement et installation de Carbon v.0.9.10}{27}{subsection.5.2.5}
\contentsline {subsection}{\numberline {5.2.6}T\IeC {\'e}l\IeC {\'e}chargement et installation de Graphite-Web v.0.9.10}{27}{subsection.5.2.6}
\contentsline {section}{\numberline {5.3}Configuration de Graphite}{27}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Configuration de l'h\IeC {\^o}te virtuel de Graphite-Web}{29}{subsection.5.3.1}
\contentsline {section}{\numberline {5.4}Configuration de Carbon}{30}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Script de d\IeC {\'e}marrage Carbon}{30}{subsection.5.4.1}
\contentsline {chapter}{\numberline {6}D\IeC {\'e}finitions des h\IeC {\^o}tes}{33}{chapter.6}
\contentsline {chapter}{\numberline {7}Astuces}{35}{chapter.7}
\contentsline {section}{\numberline {7.1}G\IeC {\'e}n\IeC {\'e}rale}{35}{section.7.1}
\contentsline {section}{\numberline {7.2}R\IeC {\'e}seau}{35}{section.7.2}
\contentsline {section}{\numberline {7.3}SSH}{36}{section.7.3}
\contentsline {section}{\numberline {7.4}SELinux}{36}{section.7.4}
\contentsline {chapter}{\numberline {8}Indices and tables}{37}{chapter.8}
