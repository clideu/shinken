.. Sphinx documentation master file, created by
   sphinx-quickstart on Tue Sep 29 19:00:49 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sphinx's documentation!
==================================

.. toctree::
   :maxdepth: 2

   centos-install
   shinken-install
   shinken-mongodb
   modules/index
   shinken-graphite-install
   shinken-cfg-host
   tooltips


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

