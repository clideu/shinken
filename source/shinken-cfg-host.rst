Définitions des hôtes
=====================

* ``shinken.cfg`` ::

define host {
	use					linux-server, linux-ssh
	contact_groups		admins
	host_name			shinken
	alias					Serveur de supervision Shinken
	address				127.0.0.1
	icon_set				server
}

* 
