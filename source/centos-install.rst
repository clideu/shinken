CentOS 7.0
==========

.. image:: _static/images/centos_logo.png

Installation
------------

Serveur avec GUI
~~~~~~~~~~~~~~~~

* Server with GUI / Serveur avec GUI
	+ HW Monitoring Utilities / Utilitaire de surveillance matériel
	+ KDE
	+ Remote Management for Linux / Gestion distante Linux
			  
Alternative : Serveur sans GUI
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		
* Minimal
	  
Installation des dernières mise à jour
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Installation des dernières mise à jour du système à la suite de la nouvelle installation.

.. code-block:: shell

	[root@localhost ~]# yum update
	[root@localhost ~]# yum -y upgrade

CentOS minimal version - Installation d'applications utiles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. note:: La version minimale de CentOS ne contient que le strict minimum au niveau application. 
	Certaines applications sont installées :
	
	+ **nano** - Alternative à l'éditeur *vi*
	+ **net-tools** - Outils réseaux, p.ex. *ifconfig*
	+ **wget** - Outils de téléchargement Web
	+ **redhat-lsb** - Suite d'outils et d'applications propre à RedHat, p.ex. *log_warning_msg*
	+ **bind-utils** - Outils de résolution DNS, p.ex. *nslookup*
	+ **gcc** - Compilateur C
	+ **perl-devel** - Ensemble des modules de dévelopeur Perl
	+ **python-devel** - Ensemble des modules de développeur Python
	  
.. code-block:: shell

	[root@localhost ~]# yum install -y nano net-tools wget redhat-lsb \ 
		bind-utils gcc perl-devel python-devel

Configuration
-------------

Désactiver SELinux
~~~~~~~~~~~~~~~~~~
	
* 	Ouvrir le fichier ``/etc/sysconfig/selinux``

.. code-block:: shell

	[root@localhost ~]# nano /etc/sysconfig/selinux

* Modifier les valeurs comme indiqué ci-dessous :

	Par défaut : ::

		SELINUX=enforcing

	Changer en : ::

		SELINUX=disabled

CentOS minimal version - Utilisateur shinken
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
* Création de l'utilisateur **shinken**

.. code-block:: shell

	[root@localhost ~]# adduser shinken

* Associer un mot de passe à l'utilisateur nouvellement créé

.. code-block:: shell
  
  	[root@localhost ~]# passwd shinken

* Attribuer des droits administrateurs limités (*wheel*) à l'utilisateur **shinken**

.. code-block:: shell

	[root@localhost ~]# gpasswd -a shinken wheel

Installation et configuration du client NTP
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Installation des paquets NTP

.. code-block:: shell
  
	[root@localhost ~]# yum -y install ntp ntpdate ntp-doc

* Ajouter Shinken au démarrage de CentOS

.. code-block:: shell

	[root@localhost ~]# chkconfig ntpd on

* Synchronisation du client avec le serveur de temps **ch.pool.ntp.org**

.. code-block:: shell
  	
  	[root@localhost ~]# ntpdate ch.pool.ntp.org

* Redémarrer le daemon du service NTP

.. code-block:: shell
  	
  	[root@localhost ~]# service ntpd restart


Remplacer firewalld par iptables
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CentOS 7.0 utilise principalement le firewalld comme pare-feu système.

.. image:: _static/images/firewall_stack.png
	:scale: 50%
	:align: center

1. Désactiver le service *firewalld*

.. code-block:: shell

	[root@localhost ~]# systemctl mask firewalld

2. Stopper le service *firewalld*

.. code-block:: shell

	[root@localhost ~]# systemctl stop firewalld

3. Installer le service *iptables*

.. code-block:: shell

	[root@localhost ~]# yum -y install iptables-services

4. Lancer *iptables* au démarrage du système

.. code-block:: shell

	[root@localhost ~]# systemctl enable iptables

5. Démarrer manuellement le service *itpables*

.. code-block:: shell

	[root@localhost ~]# systemctl start iptables


* Vérifier l'ouverture des ports

.. code-block:: shell

	[root@localhost ~] netstat -tulpn | less

* Initialiser les tables *iptables*

.. code-block:: shell

	[root@localhost ~] iptables -L -n


Désactiver IPv6 sur toutes les interfaces
-----------------------------------------

* Ouvrir le fichier ``/etc/sysctl.conf``

.. code-block:: shell

	[root@localhost ~] nano /etc/sysctl.conf

* Ajouter la ligne suivante dans le fichier

::

	net.ipv6.conf.all.disable_ipv6 = 1

* Sauvegarder (**F3**, **ENTER**) et fermer l'éditeur (**F2**)
  
* Afficher la nouvelle configuration

.. code-block:: shell

   [root@localhost ~] sysctl -p

Définition d'une adresse IP statique
------------------------------------

* Ouvrir le fichier de configuration de l'interface enp0s25 ``/etc/sysconfig/network-scripts/enp0s25``

.. code-block:: shell

   [root@localhost ~] nano /etc/sysconfig/network-scripts/ifcfg-enp0s25

::

   TYPE="Ethernet"
   BOOTPROTO="dhcp"
   DEFROUTE="yes"
   PEERDNS="yes"
   PEERROUTES="yes"
   IPV4_FAILURE_FATAL="no"
   IPV6INIT="yes"
   IPV6_AUTOCONF="yes"
   IPV6_DEFROUTE="yes"
   IPV6_PEERDNS="yes"
   IPV6_PEERROUTES="yes"
   IPV6_FAILURE_FATAL="no"
   NAME="enp0s25"
   UUID="d0be5fe8-e245-4770-b418-430d95bd67c6"
   DEVICE="enp0s25"
   ONBOOT="yes"


::

   TYPE="Ethernet"
   BOOTPROTO="static"
   IPADDR=10.2.1.5
   NETMASK=255.255.0.0
   NM_CONTROLLED=no
   DEFROUTE="yes"
   PEERDNS="yes"
   PEERROUTES="yes"
   IPV4_FAILURE_FATAL="no"
   IPV6INIT="yes"
   IPV6_AUTOCONF="yes"
   IPV6_DEFROUTE="yes"
   IPV6_PEERDNS="yes"
   IPV6_PEERROUTES="yes"
   IPV6_FAILURE_FATAL="no"
   NAME="enp0s25"
   UUID="d0be5fe8-e245-4770-b418-430d95bd67c6"
   DEVICE="enp0s25"
   ONBOOT="yes"


* Redémarrer le service de gestion réseaux

.. code-block:: shell

   [root@localhost ~] systemctl restart network.service

