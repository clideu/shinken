Shinken 2.4
===========

.. image:: _static/images/shinken_logo.png

Définition
----------
`Shinken`_ est une application de supervision d'hôtes et de services, calqué sur `Nagios`_.

.. _Shinken: http://shinken-monitoring.org/
.. _Nagios: http://www.nagios.org/  

Installation
------------

Installation des dépendances liés à Shinken
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La commande suivante installera les dépendances suivantes:
	+ Python-Curl
	+ Python-Setuptools
	+ Python-Crypto

.. code-block:: shell

	[root@localhost shinken]$ yum -y install python-pycurl python-setuptools pycrypto gcc

Installation de PIP, puis de Shinken (2.4) via PIP
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Ajouter le *repository* EPEL

.. code-block:: shell

	[root@localhost shinken]$ rpm -iUvh http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm
	[root@localhost shinken]$ yum -y update
	[root@localhost shinken]$ yum -y install python-pip 
	[root@localhost shinken]$ pip install --upgrade pip
	[root@localhost shinken]$ pip install shinken

Configuration
-------------

Ajouter Shinken au démarrage de CentOS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: shell

	[root@localhost shinken]$ chkconfig shinken on

Démarrer Shinken
~~~~~~~~~~~~~~~~

* Méthode 1

.. code-block:: shell

	[root@localhost shinken]$ service shinken start

* Méthode 2

.. code-block:: shell

	[shinken@localhost ~]$ /etc/init.d/shinken start

Liste des commandes disponible avec Shinken
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: shell

	[shinken@localhost  ~]$ shinken -l
		Available commands:
		desc:
			desc : List this object type properties
		doc:
			doc-compile : Compile the doc before enabling it online
			doc-serve : Publish the online doc on this server
		shinkenio:
			install : Grab and install a package from shinken.io
			inventory : List locally installed packages
			publish : Publish a package on shinken.io. Valid api key required
			search : Search a package on shinken.io by looking at its keywords
			update : Grab and update a package from shinken.io. Only the code and doc, NOT the configuration part! Do not update an not installed package.


Initialiser Shinken
~~~~~~~~~~~~~~~~~~~

Cette opération initialisera les chemins vers le répertoire Shinken depuis le dossier de travail du compte ``/home/shinken/``

.. code-block:: shell

	[shinken@localhost ~]$ shinken --init

.. warning:: À ce stade, lors du redémarrage de la machine, une erreur se présentera lors du démarrage des processus associés à Shinken (voir ci-dessous).

Problème avec Shinken 2.4: Après reboot, le service ne démarre pas 
------------------------------------------------------------------

(Issues #1664@GitHub) https://github.com/naparuba/shinken/issues/1624

Le problème provient du script d'initialisation ``/etc/init.d/shinken`` qui vérifie et change si besoin les droits des dossiers du *daemon* (processus)

* Ouvrir le fichier ``/etc/init.d/shinken``
  
.. code-block:: shell

	[shinken@localhost ~]$ sudo nano /etc/init.d/shinken

* Rechercher le code suivant :

.. code-block:: shell

	[ ! -d $VAR ] && mkdir -p $VAR && chown $SHINKENUSER:$SHINKENGROUP $VAR
	[ ! -d $RUN ] && mkdir -p $RUN && chown $SHINKENUSER:$SHINKENGROUP $RUN

* Remplacer les variables ``$SHINKENUSER:$SHINKENGROUP`` par ``shinken:shinken``

.. code-block:: shell

	[ ! -d $VAR ] && mkdir -p $VAR && chown shinken:shinken $VAR
	[ ! -d $RUN ] && mkdir -p $RUN && chown shinken:shinken $RUN

* Sauvegarder (**F3**, **ENTER**) et fermer l'éditeur (**F2**)

* Vérification de la configuration de Shinken
  
.. code-block:: shell
  
  [shinken@localhost ~]$ shinken-arbiter -v -c /etc/shinken/shinken.cfg

