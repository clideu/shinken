webui2
------

Description
~~~~~~~~~~~

Interface Web utilisateur du service de supervision Shinken, version 2.0.1.

`Repertoire officiel GitHub du module`_

.. _Repertoire officiel GitHub du module: https://github.com/shinken-monitoring/mod-webui

Installation
~~~~~~~~~~~~

.. important:: Le module webui2 nécessite l'installation au préalable d'une base de données, `MongoDB`_ ou SQLite.
	Contrairement à la précédente version de webui, celle-ci possède certains modules en interne comme, par exemple,
	``mongodb``, ``sqlitedb`` ou ``auth-cfg-password``.

.. _MongoDB: 02-shinken-mongodb.html#installation

* Installer les dépendances
	+ **bottle** 	(==0.12.8)
	+ **pymongo**	(>=3.0.3)
	+ **requests**
	+ **arrow**

.. code-block:: shell

	[shinken@localhost ~]$ sudo pip install 'bottle==0.12.8' 'pymongo>=3.0.3' requests arrow

* OPT.: Vérifier la présence de **webui2** dans shinken.io

.. code-block:: shell

	[shinken@localhost ~]$ shinken search webui2

* Installer le module **webui2**

.. code-block:: shell

	[shinken@localhost ~]$ shinken install webui2

* Afficher la configuration du module **webui2** et confirmer le nom du module

.. code-block:: shell

	[shinken@localhost ~]$ cat /etc/shinken/modules/webui2.cfg | grep webui
		module_name    webui2
	   module_type    webui2

* Ouvrir le fichier de configuration ``/etc/shinken/brokers/broker-master.cfg``

.. code-block:: shell

	[shinken@localhost ~]$ sudo nano /etc/shinken/brokers/broker-master.cfg

* Ajouter le module WebUI2 à la configuration Shinken (attention à la case)

::

	modules     webui2

* Sauvegarder (**F3**, **ENTER**) et fermer *nano* (**F2**)

* Redémarrer le service Shinken

.. code-block:: shell

	[shinken@localhost ~]$ sudo service shinken restart

Configuration
~~~~~~~~~~~~~
	
* Ouvrir le port 7767 de **webui2** dans **iptables**

.. code-block:: shell

	[root@localhost shinken]$ nano /etc/sysconfig/iptables

.. warning:: Par défaut, CentOS 7.0 utilise **firewalld**. Voir `Remplacer "firewalld" par "iptables"`_ pour le remplacer par *iptables*.
 	
.. _Remplacer "firewalld" par "iptables": ../centos-install.html#remplacer-firewalld-par-iptables

* Ajouter la ligne suivante dans le fichier ``/etc/sysconfig/iptables``

::

 	-A INPUT -m state --state NEW -m tcp -p tcp --dport 7767 -j ACCEPT

* Sauvegarder la configuration (**F2**, **ENTER**) et fermer l'éditeur (**F3**)

* Recharger les tables du pare-feu **iptables**

.. code-block:: shell
  
  	[root@localhost shinken]$ iptables-restore < /etc/sysconfig/iptables

* Initialiser les tables **iptables**

.. code-block:: shell

	[root@localhost shinken] iptables -L -n