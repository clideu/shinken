linux-ssh
=========

Description
-----------

Ce module `Shinken`_ permet l'établissement de connexion SSH vers des hôtes Linux afin de receuillir des informations systèmes. 
Le module est présent sur `shinken.io`_ et a été développé par `naparabua`_, alias Jean Gabès, principal développeur de la solution `Shinken`_.

.. _shinken.io : http://shinken.io/
.. _naparabua : http://shinken.io/~naparuba
.. _Nagios: http://www.nagios.org/
.. _Shinken: http://www.shinken-monitoring.org/ 

Installation du module
----------------------

* Installation des dépendances du module linux-ssh
	+ Python-Paramiko
	+ Sysstat
	+ Ntp

.. code-block:: shell

	[shinken@localhost ~]$ sudo yum -y install python-paramiko sysstat ntp

* OPT.: Vérifier la présence du module **linux-ssh** dans shinken.io

.. code-block:: shell

	[shinken@localhost ~]$ shinken search linux-ssh

* Installer le module Shinken **linux-ssh**

.. code-block:: shell

	[shinken@localhost ~]$ shinken install linux-ssh

Configuration du module
-----------------------

.. note:: Il est nécessaire d'installer la clé publique RSA du serveur Shinken sur les différents hôtes afin d'autoriser Shinken à se connecter en SSH auprès de ces derniers dans le but de récupérer des informations, 

* Génération de la paire de clés RSA utilisées par le client SSH de Shinken

.. code-block:: shell

	[shinken@localhost ~]$ ssh-keygen

Laisser les champs vides et poursuivre avec la touche **ENTER**

* Copier la clé nouvellement créée vers le client à monitorer afin qu'il autorise la connexion SSH de Shinken.

.. code-block:: shell

	[shinken@localhost ~]$ ssh-copy-id -i ~/.ssh/id_rsa shinken@localhost

Dans cet exemple, ``shinken@localhost`` est l'utilisateur (shinken) et la machine cliente (localhost) qui sera monitorée par Shinken.

.. code-block:: shell

	The authenticity of host 'localhost (::1)' can't be established.
	ECDSA key fingerprint is XX:XX:[...]:XX.
	Are you sure you want to continue connecting (yes/no)? yes

Répondre **yes** à la question afin de valider la connexion SSH.

.. code-block:: shell

	/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
	/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
	shinken@localhost's password: 

Saisir le mot de passe de l'utilisateur **shinken** de la machine cible **localhost**.

.. code-block:: shell

	Number of key(s) added: 1

	Now try logging into the machine, with:   "ssh 'shinken@localhost'"
	and check to make sure that only the key(s) you wanted were added.

.. note:: Cette manipulation a pour but de copier la clé publique RSA vers les machines distantes qui seront monitorées.
 	Une fois la connexion établie, Shinken récupère l'ensemble des informations définies dans le service *linux-ssh*.

* Vérifier le bon fonctionnement d'un des scripts du module **linux-ssh**: **check_load_average_by_ssh.py**

.. code-block:: shell

	[shinken@localhost ~]$ /var/lib/shinken/libexec/check_load_average_by_ssh.py -H localhost -i ~/.ssh/id_rsa

Ajout du service au fichier de configuration de l'hôte
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  
* Ouvrir le fichier de configuration de l'hôte

.. code-block:: shell

	[shinken@localhost ~]$ nano /etc/shinken/hosts/hostname.cfg

Remplacer ``hostname.cfg`` par celui de l'hôte.

* Ajouter la chaîne de caractère **linux-ssh** dans le champ **use**

	Avant : ::

		use 		linux

	Après : ::

		use 		linux, linux-ssh

* Sauvegarder le fichier (**F2**, **ENTER**) et fermer l'éditeur (**F3**)

* Redémarrer le service Shinken

.. code-block:: shell

	[shinken@localhost ~]$ sudo service shinken restart