Monitoring-Plugins
------------------

Description
~~~~~~~~~~~

	La suite de modules `Monitoring-Plugins`_ est un projet regroupant des modules compatibles et utilisables par de nombreuses solutions de supervision, comme par exemple `Nagios`_ ou `Shinken`_. Ces modules sont majoritairement codés en C, Perl ou Python, développé et entretenu par la communauté et les membres du projet `Monitoring-Plugins`_.

	.. _Monitoring-Plugins: https://www.monitoring-plugins.org/
	.. _Nagios: http://www.nagios.org/
	.. _Shinken: http://www.shinken-monitoring.org/ 

Installation
~~~~~~~~~~~~

* Téléchargement de la suite de plugins **monitor-plugins** 2.1.1

.. code-block:: shell

	[shinken@localhost ~]$ wget https://www.monitoring-plugins.org/download/monitoring-plugins-2.1.1.tar.gz

* Décompresser l'archive **monitoring-plugins.2.1.1.tar.gz**

.. code-block:: shell
  	
  	[shinken@localhost ~]$ tar -xvzf monitoring-plugins.2.1.1.tar.gz

* Ouvrir le dossier et lancer la configuration et l'installation

.. code-block:: shell
  	
  	[shinken@localhost ~]$ sudo yum -y install perl-devel
  	[shinken@localhost ~]$ cd monitoring-plugins-2.1.1
  	[shinken@localhost monitoring-plugins-2.1.1]$ ./configure --with-nagios-user=shinken --with-nagios-group=shinken --enable-libtap --enable-extra-opts --enable-perl-modules --libexecdir=/var/lib/shinken/libexec
  	[shinken@localhost monitoring-plugins-2.1.1]$ make
  	[shinken@localhost monitoring-plugins-2.1.1]$ sudo make install

.. warning:: 
	L'installation de ces modules nécessite d'avoir les applications systèmes dépendantes installées au préalable. Par exemple, la commande ``check_dns`` fait appel à ``nslookup`` pour réaliser ses tests de résolutions. En cas d'absence de l'application ou du programme, le compilateur ignorera le problème.

Normalement, les plugins sont installés dans le repértoire de Nagios. En cas d'utilisation de plugins de Nagios, Shinken regarde dans le répertoire de plugin de Nagios ``/usr/lib/nagios/plugins``. Dans notre installation, les plugins de Nagios sont installés dans le même que ceux de Shinken ``/var/lib/shinken/libexec``. Il est donc nécessaire d'indiquer à Shinken l'emplacement de ces plugins.

Configuration
~~~~~~~~~~~~~

* Ouvrir le fichier contenant les variables de chemins des ressources Shinken

.. code-block:: shell
  
	[shinken@localhost ~]$ sudo nano /etc/shinken/resource.d/paths.cfg

::

	# Nagios legacy macros
	$USER1$=$NAGIOSPLUGINSDIR$
	$NAGIOSPLUGINSDIR$=/usr/lib/nagios/plugins

	#-- Location of the plugins for Shinken
	$PLUGINSDIR$=/var/lib/shinken/libexec

* Remplacer la ligne ``$NAGIOSPLUGINSDIR$=/usr/lib/nagios/plugins`` par ``$NAGIOSPLUGINSDIR$=$PLUGINSDIR$``
  
::

	# Nagios legacy macros
	$USER1$=$NAGIOSPLUGINSDIR$
	$NAGIOSPLUGINSDIR$=$PLUGINSDIR$

	#-- Location of the plugins for Shinken
	$PLUGINSDIR$=/var/lib/shinken/libexec

* Redémarrer le service Shinken

.. code-block:: shell

	[shinken@localhost ~]$ sudo service shinken restart