linux-snmp
==========

Description
-----------

Ce module `Shinken`_ permet l'utilisation d'un service basé sur le protocole SNMP afin de receuillir des informations sur des hôtes Linux. 
Le module est présent sur `shinken.io`_ et a été développé par `naparabua`_, alias Jean Gabès, principal développeur de la solution `Shinken`_.

.. _Nagios: http://www.nagios.org/
.. _Shinken: http://www.shinken-monitoring.org/ 
.. _shinken.io : http://shinken.io/
.. _naparabua : http://shinken.io/~naparuba
 
Installation du module
----------------------

* OPT.: Vérifier la présence du module **linux-snmp** dans shinken.io

.. code-block:: shell

	[shinken@localhost ~]$ shinken search linux-snmp

* Installer le module Shinken **linux-snmp**

.. code-block:: shell

	[shinken@localhost ~]$ shinken install linux-snmp

Ajout du service au fichier de configuration de l'hôte
------------------------------------------------------
  
* Ouvrir le fichier de configuration de l'hôte

.. code-block:: shell

	[shinken@localhost ~]$ nano /etc/shinken/hosts/hostname.cfg

Remplacer ``hostname.cfg`` par celui de l'hôte.

* Ajouter la chaîne de caractère **linux-ssh** dans le champ **use**

	Avant : ::

		use 		linux

	Après : ::

		use 		linux, linux-snmp

* Sauvegarder le fichier (**F2**, **ENTER**) et fermer l'éditeur (**F3**)

* Redémarrer le service Shinken

.. code-block:: shell

	[shinken@localhost ~]$ sudo service shinken restart
