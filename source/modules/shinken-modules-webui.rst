webui
=====

.. note:: Une nouvelle version d'interface utilisateur Web indépendante de la précédente a vu le jour, `webui2`_ (2.0.1). 
	Elle offre les mêmes possibilités que webui, intègrant de base plusieurs modules. L'installation est décrite au `point suivant`_.

.. _webui2: https://github.com/shinken-monitoring/mod-webui/wiki
.. _point suivant: modules/shinken-modules-webui2

Description
-----------

Interface web utilisateur du service de supervision Shinken.

Installation
------------

* Installation du module **webui**

.. code-block:: shell

	[shinken@localhost ~]$ shinken install webui

* Ouvrir le fichier de configuration du *broker* ``/etc/shinken/brokers/broker-master.cfg``

.. code-block:: shell

	[shinken@localhost ~]$ sudo nano /etc/shinken/brokers/broker-master.cfg

* Ajouter le module **webui** (attention à la case)

::

	modules     webui

* Sauvegarder (**F3**, **ENTER**) et fermer l'éditeur (**F2**)

* Redémarrer le service Shinken

.. code-block:: shell

	[shinken@localhost ~]$ sudo service shinken restart

* Vérifier le fonctionnement du service Web de l'interface Shinken
  
.. note:: Le module webui exécute un service Web Python qui écoute sur le port 7767 (port par défaut défini dans ``/etc/shinken/modules/webui.cfg``)

.. code-block:: shell

	[shinken@srv-shinken ~]$ sudo netstat -tulpn | grep 7767
	tcp      0      0 0.0.0.0:7767            0.0.0.0:*               LISTEN      19902/python2.7

À ce stade, le pare-feu iptables bloque les connexions vers ce port. Il est donc nécessaire de l'ouvrir.

Configuration
-------------

Ouverture du port du pare-feu
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
* Ouvrir le port 7767 de ``webui`` dans ``iptables``

.. code-block:: shell

	[root@localhost shinken]$ nano /etc/sysconfig/iptables

.. warning:: Par défaut, CentOS 7.0 utilise **firewalld**. Voir `Remplacer "firewalld" par "iptables"`_ pour le remplacer par *iptables*.
 	
.. _Remplacer "firewalld" par "iptables": ../centos-install.html#remplacer-firewalld-par-iptables

* Ajouter la ligne suivante dans le fichier ``/etc/sysconfig/iptables``

::

 	-A INPUT -m state --state NEW -m tcp -p tcp --dport 7767 -j ACCEPT

* Sauvegarder la configuration (**F2**, **ENTER**) et fermer l'éditeur (**F3**)

* Recharger les tables du pare-feu ``iptables``

.. code-block:: shell
  
  	[root@localhost shinken]$ iptables-restore < /etc/sysconfig/iptables

* Initialiser les tables ``iptables``

.. code-block:: shell

	[root@localhost shinken] iptables -L -n

.. note:: En tentant de se connecter à ce stade à l'interface Web http://localhost:7767 avec le nom d'utilisateur et mot de passe par défaut, admin / admin, une erreur se produit.
	Cette dernière est due à l'absence de module d'authentification.

Installation et configuration du module auth-cfg-password
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ce module permet l'authentification selon les informations définies statiquement dans les fichiers de contacts ``/etc/shinken/contacts``.

* Installation du module ``auth-cfg-password``

.. code-block:: shell
 
 	[shinken@localhost ~]$ shinken install auth-cfg-password

* Ouvrir le fichier de configuration ``/etc/shinken/modules/webui.cfg``

.. code-block:: shell
  
	[shinken@localhost ~]$ sudo nano /etc/shinken/modules/webui.cfg

* Ajouter le module ``auth-cfg-password`` dans la configuration de webui (attention à la case)

::

	 modules     auth-cfg-password

* Sauvegarder (**F3**, **ENTER**) et fermer l'éditeur (**F2**)

* Redémarrer le service Shinken

.. code-block:: shell

	[shinken@localhost ~]$ sudo service shinken restart

.. note:: Il est désormais possible de se conencter à l'interface Web de Shinken. Néanmoins, une erreur se manifeste en tentant d'ouvrir l'onglet "Dashboard".
	Pour y remmédier, il suffit d'installer et de configurer soit SQLite ou MongoDB.

Installation et configuration du module sqlite
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ce module met un place une base de données SQLite qui permettra de sauvegarder les préférences utilisateurs de l'interface Web Shinken.

* Installation du module ``sqlitedb``

.. code-block:: shell

	[shinken@localhost ~]$ shinken install squlitedb

* Vérifier le nom du module ``sqlitedb``

.. code-block:: shell
  
	[shinken@localhost ~]$ cat /etc/shinken/modules/sqlitedb.cfg
   define module {
      module_name     SQLitedb
      module_type     sqlitedb
      uri             /var/lib/shinken/webui.db
   }
* Ajouter le module ``SQLitedb`` dans la configuration de webui (attention à la case)

::

	 modules     auth-cfg-password, SQLitedb

* Sauvegarder (**F3**, **ENTER**) et fermer l'éditeur (**F2**)

* Redémarrer le service Shinken

.. code-block:: shell

	[shinken@localhost ~]$ sudo service shinken restart

.. note:: L'onglet "Dashboard" de l'interface Web fonctionne normalement. Mais une erreur se produit en sélectionnant l'onglet "Wall".
	Cette erreur est due à une librairie obsolète utilisée par le sous-module ``wall.py``, ``helper``.

* Modification du fichier ``/var/lib/shinken/modules/webui/plugins/wall/wall.py``

.. code-block:: shell
  
	[shinken@localhost ~]$ sudo nano /var/lib/shinken/modules/webui/plugins/wall/wall.py

.. code-block:: shell
	:lineno-start: 31

	from helper import hst_srv_sort

À modifier en :

.. code-block:: shell
	:lineno-start: 31

	from shinken.misc.sorter import hst_srv_sort