MongoDB 3.0
===========

.. image:: _static/images/mongodb_logo.png

Définition
----------

Base de données orientée objet, utilisé avec le module Shinken **webui2** afin de stocker les profils utilisateurs et/ou les journaux systèmes (*logs*).

Installation
------------

CentOS ne possède pas de dépôt initialement pour l'installation de MongoDB. Deux possibilités pour l'installation :

	+ Depuis les fichiers sources
	+ Depuis un dépôt à disposition des distribution de la famille RedHat
	  
L'installation depuis un dépôt évite de nombreux désagrèment de dépendances non-résolus. L'installation sera réalisée avec cette méthode.

`Tutoriel officiel MongoDB sur système RedHat/CentOS`_

.. _Tutoriel officiel MongoDB sur système RedHat/CentOS: http://docs.mongodb.org/manual/tutorial/install-mongodb-on-red-hat/

.. Important:: Attention à préserver de l'espace disque afin d'éviter des erreurs d'initialisation de la base de données. 
	Avec un espace disque faible, MangoDB ne démarrera pas.

* Créer le fichier ``/etc/yum.repos.d/mongodb-org-3.0.repo`` afin d'ajouter le dépôt EPEL à la distribution.

.. code-block:: shell

	[root@localhost ~]# nano /etc/yum.repos.d/mongodb-org-3.0.repo

* Insérer les lignes ci-dessous dans le fichier, puis sauvegarder sous (**F3**, **ENTER**) et fermer le fichier (**F2**)

::

	[mongodb-org-3.0]
	name=MongoDB Repository
	baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.0/x86_64/
	gpgcheck=0
	enabled=1

* Mettre à jour les dépôts

.. code-block:: shell

	[root@localhost ~]# yum -y udpate

* Installer MongoDB et l'extension Python associée 

.. code-block:: shell

	[root@localhost ~]# yum -y install mongodb-org pymongo


Configuration
-------------

Aucune configuration en particulier n'est nécessaire, en dehors de la planification du démarrage système de l'application.

* Démarrage manuel de MongoDB

.. code-block:: shell

	[shinken@localhost ~]$ sudo service mongod start

* Démarrage système de MongoDB

.. code-block:: shell

	[shinken@localhost ~]$ sudo chkconfig mongod on


* Vérifier le status de MongoDB

.. code-block:: shell

	[shinken@localhost ~]$ mongostat