Astuces
=======

Ci-dessous une liste non-exhaustive de commandes utiles et communes à la plupart des distributions Linux.

Générale
--------
	
* Création d'un utilisateur

.. code-block:: shell

	[root@linux ~]$ adduser *username*

* Associer ou changer le mot de passe d'un utilisateur

.. code-block:: shell

  	[root@linux ~]$ passwd *username*

* Attribuer des droits administrateurs limités (*wheel*) à un utilisateur

.. code-block:: shell

	[root@linux ~]$ gpasswd -a nom_utilisateur wheel

* Passer sur un autre compte

.. code-block:: shell
  
	[root@linux ~]$ su *username*

* Passer sur le compte superutilisateur

.. code-block:: shell
  
  	[user@linux ~]$ su

* Lister le contenu du repértoire courant (visible et caché)

.. code-block:: shell
  
  	[user@linux ~]$ ls -lha .

  	+ -l (list)  : Affichage sur la forme d'une liste
  	+ -h (human) : Affichage de la taille de fichier en kilo, mega, giga octets
  	+ -a (all)   : Affichage des fichiers cachés
  
Réseau
------

* Changer le nom de la machine dans les fichiers ``/etc/hostname`` et ``/etc/hosts``

.. code-block:: shell
  
  [root@linux ~]$ nano /etc/hostname

.. code-block:: shell

  [root@linux ~]$ nano /etc/hosts

Ajouter le nom de la machine dans les deux fichiers, sauvegarder (F3, ENTER) puis fermer (F2)

SSH
---

* Réaliser une connexion SSH vers un hôte spécifique
  
.. code-block:: shell
  
  [user@linux ~]$ ssh host_user@host_address 

Ex.: Connexion vers un serveur Shinken à l'adresse ``10.10.10.50`` avec le nom d'utilisateur ``shinken``

.. code-block:: shell

  [user@linux ~] ssh shinken@10.10.10.50

Valider en acceptant la clé publique reçue qui sera stocké dans le dossier courant ``~/.ssh/known_hosts``

* Effacer la clé publique enregistrée d'un hôte spécifique

.. code-block:: shell
  
  [user@linux ~]$ ssh-keygen -R host_address [-f known_hosts_file]

Ex.: Effacer la clé publique enregistrée du serveur **10.10.10.50**

.. code-block:: shell

  [user@linux ~] ssh-keygen -R 10.10.10.50

.. note:: L'option **-f** est optionnelle; elle permet d'indiquer le chemin vers le fichier contenant les hôtes connus


SELinux
-------
	
* Désactivation du module SELinux

.. code-block:: shell

	[root@linux ~]$ nano /etc/sysconfig/selinux

Par défaut :
	*SELINUX=enforcing*

Changer en :
	*SELINUX=disabled*