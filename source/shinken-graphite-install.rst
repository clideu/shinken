Graphite 0.9.10
===============

Définition
----------

Graphite est une application permettant l'affichage de données sous forme de graphiques. Elle s'exécute autour de trois principaux programmes:

	+ `Whisper`_ pour le stockage des données numériques
	+ `Carbon`_ comme processus d'écoute et récupération des données (*backend*)
	+ `Graphite-Web`_ comme interface Web basée sur Django pour le rendu graphique des données (*frontend*)

.. _Carbon: http://graphite.readthedocs.org/en/latest/carbon-daemons.html
.. _Graphite-Web: http://graphite.readthedocs.org/en/latest/index.html

.. image:: _static/images/graphite_overview.png

Installation
------------

`Documentation officiel Graphite`_

.. _Documentation officiel Graphite: http://graphite.readthedocs.org/en/latest/install.html

.. note:: Sur le site de Graphite, il est possible de récupérer un script Python, ``check-dependencies.py``, qui vérifie si les dépendances sont remplies ou pas. `Lien vers le script Python`_

.. _Lien vers le script Python: https://launchpad.net/graphite/0.9/0.9.10/+download/check-dependencies.py

Installation des dépendances (via YUM)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	+ `Pycairo`_         : Interface Python pour la librairie graphique Cairo
	+ `python-memcached`_: Interface pour la mémoire cache de processus Python
	+ `python-ldap`_     : Client LDAP en Python
	+ `django-tagging`_  : Permet l'utilisation de tag par Django
	+ `fontconfig`_ 		: Paquet de police
	+ `httpd-devel`_ 		: Serveur Apache2, variante CentOS/RedHat, mode développeur (nécessaire à la compilation de ``mod_python``)
	+ `Module WSGI`_     : Module WSGI du serveur Apache2
	+ `flex`_ 				: Librarie pour la reconnaissance lexicale (nécessaire à la compilation de ``mod_python``)
	

.. _Pycairo: http://www.cairographics.org/pycairo/
.. _python-memcached: https://pypi.python.org/pypi/python-memcached

.. _python-ldap: http://www.python-ldap.org/index.html
.. _django-tagging: http://code.google.com/p/django-tagging/
.. _fontconfig: http://www.freedesktop.org/wiki/Software/fontconfig/
.. _httpd-devel: http://linuxsoft.cern.ch/cern/slc63/i386/yum/updates/repoview/httpd-devel.html
.. _Module WSGI: https://code.google.com/p/modwsgi/
.. _flex: http://flex.sourceforge.net/

.. code-block:: shell

	[shinken@localhost ~]$ sudo yum -y install pycairo python-memcached python-twisted-core \
		python-ldap python-django-tagging fontconfig httpd-devel mod_wsgi flex git
		


Installation des dépendances (via PIP)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	+ `Whisper`_         : Base de données à taille fixe pour le stockage de données numériques fiables
	+ `Django`_          : Framework Web Python (<=v.1.4)
	+ `Twisted`_         : Framework d'application réseau en Python, dépendance de Carbon
	+ `zope-interface`_  : Ensemble d'interfaces Python, présent dans le pack ``Twisted`` 
	  
.. warning:: Attention à bien installer une version de Django inférieur à la 1.6; le code nécessite la routine ``execute_manager`` qui est obsolète et absent à partir de la version 1.6. 
	Carbon nécessite l'installation d'une version de Twisted inférieur à la 12.0.

.. code-block:: shell

	[shinken@localhost ~]$ sudo pip install django==1.4 whisper 'twisted<12.0'

.. _Whisper: https://pypi.python.org/pypi/whisper
.. _Django: http://www.djangoproject.com/
.. _Twisted: http://twistedmatrix.com/
.. _zope-interface: http://pypi.python.org/pypi/zope.interface/

Téléchargement et installation de mod_python 3.5.0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ce module est utilisé afin d'exécuter du code pour l'interface web de Graphite.

.. note:: `mod_python`_ n'est plus présent par défaut dans CentOS à partir de la version 7. Le développement de ce module a été stoppé en 2013 et des alternatives plus performantes sont apparues comme, par exemple, `mod_wsgi`_. Il faut donc soit le télécharger depuis le site officiel de `mod_python`_, soit l'installer depuis un dépôt tier.

..	_mod_python: http://modpython.org/
.. _mod_wsgi: https://code.google.com/p/modwsgi/

* Téléchargement de l'archive source de l'application	

.. code-block:: shell

	[shinken@localhost ~]$ wget http://dist.modpython.org/dist/mod_python-3.5.0.tgz

* Décompression de l'archive dans le dossier courant 

.. code-block:: shell

	[shinken@localhost ~]$ tar -xvzf mod_python-3.5.0.tgz

* Compilation du code et installation

.. code-block:: shell

	[shinken@localhost ~]$ cd mod_python-3.5.0
	[shinken@localhost mod_python-3.5.0]$ ./configure -with-apxs=/usr/bin/apxs

.. important:: Dépendance: La compilation nécessite **apxs**, un module d'extension Apache2, se trouvant parmis les outils de développeur du serveur web Apache2 (`httpd-devel`_) ainsi que la librarie **flex**. 
	Le script de configuration ``./configure`` avertira en cas d'absence d'une dépendance.

* Lancer la compilation et l'installation
  
.. code-block:: shell

	[shinken@localhost mod_python-3.5.0]$ make
	[shinken@localhost mod_python-3.5.0]$ sudo make install

* Tester le bon fonctionnement du module
  
.. code-block:: shell

	[shinken@localhost mod_python-3.5.0]$ mod_python version

.. error:: Une erreur survient au lancement de l'application. 

	.. code-block:: shell

		[shinken@localhost mod_python-3.5.0]$ mod_python version
		Traceback (most recent call last):
		  File "/usr/local/bin/mod_python", line 30, in <module>
		    import mod_python
		  File "/usr/lib64/python2.7/site-packages/mod_python/__init__.py", line 25, in <module>
		    from . import version
		  File "/usr/lib64/python2.7/site-packages/mod_python/version.py", line 3
		    version = "fatal: Not a git repository (or any parent up to mount point /home)
		                                                                                 ^
		SyntaxError: EOL while scanning string literal

.. note:: Cette erreur est due à l'absence d'interfaces et à une vérification mal implémentée de la version du code (révision Git) lors de la compilation.
	Une erreur est susceptible d'apparaitre si le chemin vers le dossier source comporte des accentuations (compilation du code avec codage US). `- site -`_

.. _- site -: http://www.aboutmonitoring.com/compiling-apache-mod_python-module-centos-7-redhat-7/

* Ouvrir et éditer le fichier ``src/Makefile``.

.. code-block:: shell
  
	[shinken@localhost mod_python-3.5.0]$ nano src/Makefile

* Insérer le bloc ci-dessous à partir de la ligne 71

.. code-block:: c

  	include/mod_python.h` include/psp_flex.h include/psp_parser.h \
  	include/requestobject.h include/tableobject.h include/connobject.h \
  	include/finfoobject.h include/hlistobject.h include/mp_version.h \
  	include/_pspmodule.h include/psp_string.h include/serverobject.h \
  	include/util.h

Afin d'obtenir le résultat suivant :

.. code-block:: c
	:lineno-start: 66

	version.c:
		@MP_GIT_SHA=$$(git describe --always); \
		echo > version.c ; \
		echo "/* THIS FILE IS AUTO-GENERATED BY Makefile */" >> version.c ; \
		echo "#include \"mp_version.h\"" >> version.c ; \
		include/mod_python.h include/psp_flex.h include/psp_parser.h \
		include/requestobject.h include/tableobject.h include/connobject.h \
		include/finfoobject.h include/hlistobject.h include/mp_version.h \
		include/_pspmodule.h include/psp_string.h include/serverobject.h \
		include/util.h \
		echo "const char * const mp_git_sha = \"$${MP_GIT_SHA}\";" >> version.c ; \

* Sauvegarder le fichier (**F3**, **ENTER**) et fermer l'éditeur (**F2**)

* Ouvrir et éditer le fichier ``dist/version.sh``.

.. code-block:: shell
  
	[shinken@localhost mod_python-3.5.0]$ nano src/Makefile

* Commenter les lignes 6 et 8 et ajouter la ligne ``echo $MAJ.$MIN.$PCH-`` afin d'obtenir le résultat suivant :

.. code-block:: c
   :linenos:  

	  	#!/bin/sh
		MPV_PATH="`dirname $0`/../src/include/mp_version.h"
		MAJ=`awk '/MP_VERSION_MAJOR/ {print $3}' $MPV_PATH`
		MIN=`awk '/MP_VERSION_MINOR/ {print $3}' $MPV_PATH`
		PCH=`awk '/MP_VERSION_PATCH/ {print $3}' $MPV_PATH`
	  	#GIT=`git describe --always`

		#echo $MAJ.$MIN.$PCH-$GIT
		echo $MAJ.$MIN.$PCH-

* Sauvegarder le fichier (**F2**, **ENTER**) et fermer l'éditeur (**F3**)

* Relancer la compilation et l'installation
  
.. code-block:: shell

	[shinken@localhost mod_python-3.5.0]$ make clean
	[shinken@localhost mod_python-3.5.0]$ make
	[shinken@localhost mod_python-3.5.0]$ sudo make install

* Retester le bon fonctionnement du module

.. code-block:: shell
  
	[shinken@localhost mod_python-3.5.0]$ mod_python version

	mod_python:  3.5.0-
	             '/usr/lib64/httpd/modules/mod_python.so'

	python:      2.7.5 (default, Jun 24 2015, 00:41:19) [GCC 4.8.3 20140911 (Red Hat 4.8.3-9)]
	             '/usr/bin/python'

	httpd:       2.4.6
	             '/usr/sbin/httpd'

	apr:         1.4.8
	platform:    Linux-3.10.0-229.14.1.el7.x86_64-x86_64-with-centos-7.1.1503-Core



Téléchargement et installation de txAMQP v.0.6.2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Module utile en cas d'utilisation du protocole `AMQP`_ - *Advanced Message Queuing Protocol*.

.. _AMQP: https://www.amqp.org/

.. important:: Dépendance: **txAMQP** nécessite les interfaces Python contenu dans le paquet de développeur Python ``python-devel``.

* Téléchargement des sources de l'application

.. code-block:: shell

	[shinken@localhost ~]$ wget https://pypi.python.org/packages/source/t/txAMQP/txAMQP-0.6.2.tar.gz

* Décompression de l'archive dans le dossier courant
  
.. code-block:: shell
	  
	[shinken@localhost ~]$ tar -xvzf txAMQP-0.6.2.tar.gz

* Compilation du code et installation

.. code-block:: shell
	  
	[shinken@localhost ~]$ cd txAMQP-0.6.2
	[shinken@localhost txAMQP-0.6.2]$ sudo python setup.py install


Téléchargement et installation de Carbon v.0.9.10
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Téléchargement des sources de l'application
  
.. code-block:: shell

	[shinken@localhost ~]$ wget https://launchpad.net/graphite/0.9/0.9.10/+download/carbon-0.9.10.tar.gz

* Décompression de l'archive dans le dossier courant

.. code-block:: shell
  
	[shinken@localhost ~]$ tar -xvzf carbon-0.9.10.tar.gz

* Compilation du code et installation

.. code-block:: shell
	  
	[shinken@localhost ~]$ cd carbon-0.9.10
	[shinken@localhost carbon-0.9.10]$ sudo python setup.py install


Téléchargement et installation de Graphite-Web v.0.9.10
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Téléchargement des sources de l'application
  
.. code-block:: shell

	[shinken@localhost ~]$ wget https://launchpad.net/graphite/0.9/0.9.10/+download/graphite-web-0.9.10.tar.gz

* Décompression de l'archive dans le dossier courant

.. code-block:: shell
  
	[shinken@localhost ~]$ tar -xvzf graphite-web-0.9.10.tar.gz

* Compilation du code et installation

.. code-block:: shell
	  
	[shinken@localhost ~]$ cd graphite-web-0.9.10
	[shinken@localhost graphite-web-0.9.10]$ sudo python setup.py install


Configuration de Graphite
-------------------------

Le dossier d'installation et de configuration de graphite se situe dans ``/opt/graphite``.

* Vérification de la version du serveur Apache2 ``httpd``
  
.. code-block:: shell

	[shinken@localhost ~]$ httpd -v
	Server version: Apache/2.4.6 (CentOS)
	Server built:   Aug 24 2015 18:11:25

.. note:: La configuration de l'environnement virtuel *vHost* Python de Graphite dépend de la version d'Apache2 présente.

* Mise en place de la configuration par défaut
  
.. code-block:: shell

	[shinken@localhost ~]$ sudo cp /opt/graphite/conf/carbon.conf.example /opt/graphite/conf/carbon.conf
	[shinken@localhost ~]$ sudo cp /opt/graphite/conf/storage-schemas.conf.example /opt/graphite/conf/storage-schemas.conf

* Configuration de l'hôte virtuel *vHost*

   - Copie de la config de l'hôte virtuel de Graphite-Web dans le répertoire du serveur Apache2 ``httpd``
   - Copie de la config d'exemple pour le module web Apache2 WSGI
 
.. code-block:: shell
  
  	[shinken@localhost ~]$ sudo cp /opt/graphite/examples/example-graphite-vhost.conf /etc/httpd/conf.d/graphite.conf
  	[shinken@localhost ~]$ sudo cp /opt/graphite/conf/graphite.wsgi.example /opt/graphite/conf/graphite.wsgi

* Lancer la création de la base de données SQLite

.. code-block:: shell  

  	[shinken@localhost ~]$ cd /opt/graphite/webapp/graphite
  	[shinken@localhost graphite]$ sudo python manage.py syncdb

.. error:: Une erreur se présente lors de la création du fichier SQLite utilisé par Django, indiquant un problème au niveau du chemin vers le fichier.

* Copier l'exemple de configuration locale

.. code-block:: shell

	[shinken@localhost ~]$ sudo cp /opt/graphite/webapp/graphite/local_settings.py.example /opt/graphite/webapp/graphite/local_settings.py

.. note:: Ce fichier de configuration est complémentaire à celui chargé par défaut, ``settings.py``.

* Ouvrir le fichier avec un éditeur et modifier/décommenter les champs suivants :

.. code-block:: shell

	[shinken@localhost ~]$ sudo nano /opt/graphite/webapp/graphite/local_settings.py

.. code-block:: python
	:lineno-start: 12

	TIME_ZONE = 'Europe/Paris'

.. code-block:: python
	:lineno-start: 37

	MEMCACHE_HOSTS = ['127.0.0.1:11211']

.. code-block:: python
   :lineno-start: 141

   DATABASES = {
      'default': {
        'NAME': '/opt/graphite/storage/graphite.db',
        'ENGINE': 'django.db.backends.sqlite3',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': ''
      }
   }

* Relancer la création de la base de données SQLite

.. code-block:: shell

  	[shinken@localhost graphite]$ sudo python manage.py syncdb
	Creating tables ...
	Creating table account_profile
	Creating table account_variable
	Creating table account_view
	Creating table account_window
	Creating table account_mygraph
	Creating table dashboard_dashboard_owners
	Creating table dashboard_dashboard
	Creating table events_event
	Creating table auth_permission
	Creating table auth_group_permissions
	Creating table auth_group
	Creating table auth_user_user_permissions
	Creating table auth_user_groups
	Creating table auth_user
	Creating table django_session
	Creating table django_admin_log
	Creating table django_content_type
	Creating table tagging_tag
	Creating table tagging_taggeditem

À la question où on vous demande si vous souhaitez créer un superutilisateur pour Django, répondre "non".

.. code-block:: shell

	You just installed Django's auth system, which means you don't have any superusers defined.
	Would you like to create one now? (yes/no): no
	Installing custom SQL ...
	Installing indexes ...
	Installed 0 object(s) from 0 fixture(s)

* Modification des droits du répértoire ``/opt/graphite/storage``

.. code-block:: shell
  
	[shinken@localhost ~]$ sudo chown -R apache:apache /opt/graphite/storage/

.. note:: Cette opération permet au service Apache2 de Graphite d'interagir avec les données stockées par Whysper.

Configuration de l'hôte virtuel de Graphite-Web
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Editer le fichier de configuration de l'hôte virtuel

.. code-block:: shell

  	[shinken@localhost ~]$ sudo nano /etc/httpd/conf.d/graphite.conf

* Modifier à partir de la ligne 58 comme indiqué ci-dessous
  
.. note:: Cette modification est nécessaire avec les récentes options présentes dans Apache 2.4 et plus.

.. code-block:: shell
   :lineno-start: 53

   # The graphite.wsgi file has to be accessible by apache. It won't
   # be visible to clients because of the DocumentRoot though.
   <Directory /opt/graphite/conf/>
      #Order deny,allow
      #Allow from all
      Options All
      AllowOverride All
      Require all granted
   </Directory>

   <Directory /opt/graphite/webapp>
      Options All
      AllowOverride All
      Require all granted
   </Directory>
  
* Sauvegarder (**F3**, **ENTER**) et fermer l'éditeur (**F2**)

* Redémarrer le serveur Apache2 ``httpd``

.. code-block:: shell

	[shinken@localhost ~]$ sudo service httpd restart

* OPTIONNEL: Démarrage de l'instance Graphite-Web avec Django en mode développeur
  
.. code-block:: shell

  	[shinken@localhost ~]$ sudo /opt/graphite/bin/run-graphite-devel-server.py /opt/graphite/

.. note:: Il est possible de consulter l'interface graphique de Graphite à l'adresse http://127.0.0.1:8080. 
	Le port 8080 correspond au port de développement défini dans graphite.settings et utilisé par Django.

* Vérifier le fonctionnement du service Web de l'interface Graphite httpd (hôte virtuel port 80)

.. code-block:: shell

	[shinken@localhost ~]$ sudo netstat -tulpn | grep 80
	tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      14353/httpd

À ce stade, le pare-feu iptables bloque les connexions vers ce port. Il est donc nécessaire de l'ouvrir.

* Ouverture du port 80 dans le pare-feu ``iptables``

.. code-block:: shell

	[shinken@ocalhost ~]$ su
	[root@localhost shinken]$ nano /etc/sysconfig/iptables

.. warning:: Par défaut, CentOS 7.0 utilise **firewalld**. Voir `Remplacer "firewalld" par "iptables"`_ pour le remplacer par *iptables*.
 	
.. _Remplacer "firewalld" par "iptables": ../centos-install.html#remplacer-firewalld-par-iptables

* Ajouter la ligne suivante dans le fichier ``/etc/sysconfig/iptables``

::

 	-A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT

* Sauvegarder la configuration (**F2**, **ENTER**) et fermer l'éditeur (**F3**)

* Recharger les tables du pare-feu ``iptables``

.. code-block:: shell
  
  	[root@localhost shinken]$ iptables-restore < /etc/sysconfig/iptables

* Initialiser les tables ``iptables``

.. code-block:: shell

	[root@localhost shinken] iptables -L -n

Configuration de Carbon
-----------------------

* Démarrage manuel de l'instance Carbon

.. code-block:: shell

	[shinken@localhost ~]$ sudo /opt/graphite/bin/carbon-cache.py start

Script de démarrage Carbon
~~~~~~~~~~~~~~~~~~~~~~~~~~

* Créer le fichier ``/etc/init.d/carbon-cache``

.. code-block:: shell
  
  [shinken@localhost ~]$ sudo nano /etc/init.d/carbon-cache

* Coller la sélection suivante dans l'éditeur

.. code-block:: bash

	#!/bin/bash
	#
	# This is used to start/stop the carbon-cache daemon

	# chkconfig: - 99 01
	# description: Starts the carbon-cache daemon

	# Source function library.
	. /etc/init.d/functions

	RETVAL=0
	prog="carbon-cache"

	start_relay () {
	    /usr/bin/python /opt/graphite/bin/carbon-relay.py start
	        RETVAL=$?
	        [ $RETVAL -eq 0 ] && success || failure
	        echo
		return $RETVAL
	}

	start_cache () {
	     /usr/bin/python /opt/graphite/bin/carbon-cache.py start
	        RETVAL=$?
	        [ $RETVAL -eq 0 ] && success || failure
	        echo
		return $RETVAL
	}

	stop_relay () {
	    /usr/bin/python /opt/graphite/bin/carbon-relay.py stop
	        RETVAL=$?
	        [ $RETVAL -eq 0 ] && success || failure
	        echo
		return $RETVAL
	}

	stop_cache () {
	          /usr/bin/python /opt/graphite/bin/carbon-cache.py stop
	        RETVAL=$?
	        [ $RETVAL -eq 0 ] && success || failure
	        echo
		return $RETVAL
	}

	# See how we were called.
	case "$1" in
	  start)
	    #start_relay
	    start_cache
	        ;;
	  stop)
	    #stop_relay
	    stop_cache
		;;
	  restart)
	    #stop_relay
	    stop_cache
	    #start_relay
	    start_cache
	    ;;

	  *)
	    	echo $"Usage: $0 {start|stop}"
	        exit 2
		;;
	esac

* Sauvegarder (**F3**, **ENTER**) et fermer l'éditeur (**F2**)

* Attribuer des droits d'exécution au script ``carbon-cache``

.. code-block:: shell
  
  [shinken@localhost ~]$ sudo chmod +x /etc/init.d/carbon-cache

* Ajouter Carbon au démarrage du système

.. code-block:: shell
  
	[shinken@localhost ~]$ sudo chkconfig carbon-cache on

* Tuer tout les processus carbon-cache.py

.. code-block:: shell
  
	[shinken@localhost ~]$ sudo killall carbon-cache.py

* Démarrer manuellement à l'aide du script

.. code-block:: shell
  
  	[shinken@localhost ~]$ sudo service carbon-cache start